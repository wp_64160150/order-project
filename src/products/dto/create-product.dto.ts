import {
  IsNotEmpty,
  IsPositive,
  Length,
} from 'class-validator/types/decorator/decorators';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(5)
  name: string;
  @IsNotEmpty()
  @IsPositive()
  price: number;
}
