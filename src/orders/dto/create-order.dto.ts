import {
  IsNotEmpty,
  IsPositive,
} from 'class-validator/types/decorator/decorators';

class CreatedOrderItemDto {
  @IsNotEmpty()
  @IsPositive()
  productId: number;
  @IsNotEmpty()
  @IsPositive()
  amount: number;
}
export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;
  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];
}
