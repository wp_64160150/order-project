import { IsNotEmpty } from 'class-validator';
import {
  IsPhoneNumber,
  IsPositive,
  Length,
} from 'class-validator/types/decorator/decorators';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  age: number;
  @IsNotEmpty()
  @Length(10)
  @IsPhoneNumber()
  @IsPositive()
  tel: string;
  @IsNotEmpty()
  gender: string;
}
